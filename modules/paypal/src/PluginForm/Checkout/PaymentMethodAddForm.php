<?php

namespace Drupal\commerce_profile_inline_form_radios_paypal\PluginForm\Checkout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_paypal\PluginForm\Checkout\PaymentMethodAddForm as PaymentMethodAddFormBase;

class PaymentMethodAddForm extends PaymentMethodAddFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;

    $form['#attached']['library'][] = 'commerce_payment/payment_method_form';
    $form['#tree'] = TRUE;
    if ($payment_gateway_plugin->collectsBillingInformation()) {
      $billing_profile = $payment_method->getBillingProfile();
      if (!$billing_profile) {
        $profile_storage = $this->entityTypeManager->getStorage('profile');
        $billing_profile = $profile_storage->create([
          'type' => 'customer',
          'uid' => 0,
        ]);
      }
      $store = $this->currentStore->getStore();
      $inline_form = $this->inlineFormManager->createInstance('commerce_profile_inline_form_radios_customer_profile', [
        'profile_scope' => 'billing',
        'available_countries' => $store ? $store->getBillingCountries() : [],
        'address_book_uid' => $payment_method->getOwnerId(),
      ], $billing_profile);

      $billing_information = [
        '#parents' => array_merge($form['#parents'], ['billing_information']),
        '#inline_form' => $inline_form,
      ];

      // Unset the original billing information form element and replace it with
      // the inline form to ensure billing information is below the payment
      // method add form.
      unset($form['billing_information']);
      $form['billing_information'] = $inline_form->buildInlineForm($billing_information, $form_state);
    }

    return $form;
  }

}
