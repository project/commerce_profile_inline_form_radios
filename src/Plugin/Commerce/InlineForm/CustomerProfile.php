<?php

namespace Drupal\commerce_profile_inline_form_radios\Plugin\Commerce\InlineForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\commerce\Plugin\Commerce\InlineForm\InlineFormBase;
use Drupal\commerce_order\Plugin\Commerce\InlineForm\CustomerProfile as CustomerProfileBase;

/**
 * Provides an inline form for managing a customer profile.
 *
 * Allows copying values to and from the customer's address book.
 *
 * Supports two modes, based on the profile type setting:
 * - Single: The customer can have only a single profile of this type.
 * - Multiple: The customer can have multiple profiles of this type.
 *
 * @CommerceInlineForm(
 *   id = "commerce_profile_inline_form_radios_customer_profile",
 *   label = @Translation("Customer profile (radios)"),
 * )
 */
class CustomerProfile extends CustomerProfileBase {

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inline_form, FormStateInterface $form_state) {
    $inline_form = InlineFormBase::buildInlineForm($inline_form, $form_state);
    // Allows a widget to vary when used for billing versus shipping purposes.
    // Available in hook_field_widget_form_alter() via $context['form'].
    $inline_form['#profile_scope'] = $this->configuration['profile_scope'];

    assert($this->entity instanceof ProfileInterface);
    $profile_type_id = $this->entity->bundle();
    $allows_multiple = $this->addressBook->allowsMultiple($profile_type_id);
    $customer = $this->loadUser($this->configuration['address_book_uid']);
    $available_countries = $this->configuration['available_countries'];
    $address_book_profile = NULL;
    if ($customer->isAuthenticated() && $allows_multiple) {
      // Multiple address book profiles are allowed, prepare the dropdown.
      $address_book_profiles = $this->addressBook->loadAll($customer, $profile_type_id, $available_countries);
      if ($address_book_profiles) {
        $user_input = (array) NestedArray::getValue($form_state->getUserInput(), $inline_form['#parents']);
        if (!empty($user_input['select_address'])) {
          // An option was selected, pre-fill the profile form.
          $address_book_profile = $this->getProfileForOption($user_input['select_address'], $address_book_profiles);
        }
        elseif ($this->entity->isNew()) {
          // The customer profile form is being rendered for the first time.
          // Use the default profile to pre-fill the profile form.
          $address_book_profile = $this->selectDefaultProfile($address_book_profiles);
        }
      }

      $profile_options = $this->buildOptions($address_book_profiles);
      if ($address_book_profile) {
        $selected_option = $this->selectOptionForProfile($address_book_profile);
      }
      else {
        $selected_option = $this->entity->isNew() ? '_new' : '_original';
        // Select the address book profile, if the _original option was removed.
        if ($selected_option == '_original' && !isset($profile_options['_original'])) {
          $selected_option = $this->entity->getData('address_book_profile_id');
        }
      }

      $inline_form['#after_build'][] = [get_called_class(), 'clearValues'];
      $inline_form['addresses'] = [
        '#type' => 'container',
      ];

      if (empty($address_book_profiles)) {
        unset($profile_options['_new']);
      }

      foreach ($profile_options as $profile_option => $profile_option_label) {
        $inline_form['addresses'][$profile_option] = [
          '#type' => 'container',
        ];
        $inline_form['addresses'][$profile_option]['select_address'] = [
          '#parents' => array_merge($inline_form['#parents'], ['select_address']),
          '#type' => 'radio',
          '#title' => $profile_option_label,
          '#return_value' => $profile_option,
          '#default_value' => $selected_option,
          '#ajax' => [
            'callback' => [get_called_class(), 'ajaxRefresh'],
            'wrapper' => $inline_form['#id'],
          ],
          '#attributes' => [
            'class' => ['available-profiles'],
          ],
          '#weight' => -999,
        ];
      }
    }
    elseif ($customer->isAuthenticated() && $this->entity->isNew()) {
      // A single address book profile is allowed.
      // The customer profile form is being rendered for the first time.
      // Use the default profile to pre-fill the profile form.
      $address_book_profile = $this->addressBook->load($customer, $profile_type_id, $available_countries);
    }

    // Copy field values from the address book profile to the actual profile.
    if ($address_book_profile) {
      $this->entity->populateFromProfile($address_book_profile);
      $this->entity->unsetData('copy_to_address_book');
      $this->entity->unsetData('address_book_profile_id');
      if (!$address_book_profile->isNew()) {
        $this->entity->setData('address_book_profile_id', $address_book_profile->id());
      }
    }

    if ($this->shouldRender($inline_form, $form_state) && isset($selected_option)) {
      $view_builder = $this->entityTypeManager->getViewBuilder('profile');
      $inline_form['#rendered'] = $selected_option;
      $inline_form['addresses'][$selected_option]['rendered'] = $view_builder->view($this->entity);
      $inline_form['addresses'][$selected_option]['edit_button'] = [
        '#parents' => array_merge($inline_form['#parents'], ['edit_button']),
        '#type' => 'button',
        '#name' => $inline_form['#profile_scope'] . '_edit',
        '#value' => $this->t('Edit'),
        '#ajax' => [
          'callback' => [get_called_class(), 'ajaxRefresh'],
          'wrapper' => $inline_form['#id'],
        ],
        '#limit_validation_errors' => [$inline_form['#parents']],
        '#attributes' => [
          'class' => ['address-book-edit-button'],
        ],
      ];
    }
    else {
      // The $address_book_profile_id will be missing if the source
      // address book profile was deleted, or has never existed.
      $address_book_profile_id = $this->entity->getData('address_book_profile_id');
      $profile_storage = $this->entityTypeManager->getStorage('profile');
      if ($address_book_profile_id && !$profile_storage->load($address_book_profile_id)) {
        $this->entity->unsetData('address_book_profile_id');
      }

      $form_display = $this->loadFormDisplay();
      $form_display->buildForm($this->entity, $inline_form, $form_state);
      $inline_form = $this->prepareProfileForm($inline_form, $form_state);

      $edit = $address_book_profile ? !$address_book_profile->isNew() : !$this->entity->isNew();
      $update_on_copy = (bool) $this->entity->getData('address_book_profile_id');
      if ($allows_multiple) {
        // The copy checkbox is:
        // - Shown and checked for customers adding a new address.
        // - Shown and unchecked for admins adding a new address.
        // - Hidden and checked for customers and admins editing an address
        //   book profile which is still in sync with the current profile.
        // - Hidden and unchecked if the address book profile is no longer in
        //   sync (determined and done via the logic in buildOptions().
        $default_value = TRUE;
        if ($this->configuration['admin'] && !$edit) {
          $default_value = FALSE;
        }
        if ($edit && !$update_on_copy) {
          // The profile was originally not copied to the address book,
          // preserve that decision.
          $default_value = FALSE;
        }
        $visible = !$default_value || !$update_on_copy;
      }
      else {
        // The copy checkbox is:
        // - Hidden and checked for customers, since the address book is always
        //   supposed to reflect customer's last entered address.
        // - Shown and unchecked for admins, who need to opt-in to copying.
        $default_value = !$this->configuration['admin'];
        $visible = $this->configuration['admin'];
      }

      // Move inline form fields into the appropriate parent.
      if (isset($selected_option)) {
        $form_display = $this->loadFormDisplay();
        $form_fields = array_keys($form_display->extractFormValues($this->entity, $inline_form, $form_state));
        foreach ($form_fields as $field_name) {
          $inline_form['addresses'][$selected_option]['inline_form'][$field_name] = array_merge(
            $inline_form[$field_name],
            ['#parents' => array_merge($inline_form['#parents'], [$field_name])]
          );
          unset($inline_form[$field_name]);
        }

        $inline_form['addresses'][$selected_option]['copy_to_address_book'] = [
          '#parents' => array_merge($inline_form['#parents'], ['copy_to_address_book']),
          '#type' => 'checkbox',
          '#title' => $this->getCopyLabel($profile_type_id, $update_on_copy),
          '#default_value' => (bool) $this->entity->getData('copy_to_address_book', $default_value),
          '#weight' => 999,
          // Anonymous customers don't have an address book until they register
          // or log in, so the checkbox is not shown to them, to avoid confusion.
          '#access' => $customer->isAuthenticated() && $visible,
        ];
      }
    }

    return $inline_form;
  }

  /**
   * Ajax callback.
   */
  public static function ajaxRefresh(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $inline_form = NestedArray::getValue($form, array_slice($triggering_element['#array_parents'], 0, -3));

    return $inline_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateInlineForm(array &$inline_form, FormStateInterface $form_state) {
    InlineFormBase::validateInlineForm($inline_form, $form_state);

    if (!isset($inline_form['#rendered'])) {
      $form_display = $this->loadFormDisplay();
      $form_display->extractFormValues($this->entity, $inline_form, $form_state);
      $form_display->validateFormValues($this->entity, $inline_form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitInlineForm(array &$inline_form, FormStateInterface $form_state) {
    InlineFormBase::submitInlineForm($inline_form, $form_state);

    if (!isset($inline_form['#rendered'])) {
      $form_display = $this->loadFormDisplay();
      $form_display->extractFormValues($this->entity, $inline_form, $form_state);
      $values = $form_state->getValue($inline_form['#parents']);
      if (!empty($values['copy_to_address_book'])) {
        $this->entity->setData('copy_to_address_book', TRUE);
      }
      else {
        $this->entity->unsetData('copy_to_address_book');
      }
    }
    $this->entity->save();

    if ($this->configuration['copy_on_save'] && $this->addressBook->needsCopy($this->entity)) {
      $customer = $this->loadUser($this->configuration['address_book_uid']);
      $this->addressBook->copy($this->entity, $customer);
    }
  }

  /**
   * Determines whether the current profile should be shown rendered.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the complete form.
   *
   * @return bool
   *   TRUE if the profile should be shown rendered, FALSE otherwise.
   */
  protected function shouldRender(array $inline_form, FormStateInterface $form_state) {
    $render_parents = array_merge($inline_form['#parents'], ['render']);
    $triggering_element_name = static::getTriggeringElementName($inline_form, $form_state);
    if ($triggering_element_name == 'select_address') {
      // Reset the render flag to re-evaluate the newly selected profile.
      $form_state->set($render_parents, NULL);
    }
    elseif ($triggering_element_name == 'edit_button') {
      // The edit button was clicked, turn off profile rendering.
      $form_state->set($render_parents, FALSE);
    }
    $render = $form_state->get($render_parents);
    if (!isset($render)) {
      $render = !$this->isProfileIncomplete($this->entity);
      $form_state->set($render_parents, $render);
    }

    return $render;
  }

  /**
   * Prepares the profile form.
   *
   * @param array $profile_form
   *   The profile form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the complete form.
   *
   * @return array
   *   The prepared profile form.
   */
  protected function prepareProfileForm(array $profile_form, FormStateInterface $form_state) {
    if (!empty($profile_form['address']['widget'][0])) {
      $address_widget = &$profile_form['address']['widget'][0];
      // Remove the details wrapper from the address widget.
      $address_widget['#type'] = 'container';
      // Limit the available countries.
      $available_countries = $this->configuration['available_countries'];
      if ($available_countries) {
        $address_widget['address']['#available_countries'] = $available_countries;
      }
      // Provide a default country.
      $default_country = $this->currentCountry->getCountry();
      if ($default_country && empty($address_widget['address']['#default_value']['country_code'])) {
        $default_country = $default_country->getCountryCode();
        // The address element ensures that the default country is always
        // available, which must be avoided in this case, to prevent the
        // customer from ordering to an unsupported country.
        if (!$available_countries || in_array($default_country, $available_countries)) {
          $address_widget['address']['#default_value']['country_code'] = $default_country;
        }
      }
    }
    return $profile_form;
  }

  /**
   * Determines the name of the triggering element.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the complete form.
   *
   * @return string
   *   The name of the triggering element, if the triggering element is
   *   a part of the inline form.
   */
  protected static function getTriggeringElementName(array $inline_form, FormStateInterface $form_state) {
    $triggering_element_name = '';
    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element) {
      $parents = array_slice($triggering_element['#parents'], 0, count($inline_form['#parents']));
      if ($inline_form['#parents'] === $parents) {
        $triggering_element_name = end($triggering_element['#parents']);
      }
    }

    return $triggering_element_name;
  }

}
